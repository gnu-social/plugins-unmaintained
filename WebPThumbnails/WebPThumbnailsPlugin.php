<?php
/**
 * GNU social - a federating social network
 *
 * Plugin to make thumbnails of video files with avconv
 *
 * PHP version 5
 *
 * LICENCE: This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  Plugin
 * @package   GNUsocial
 * @author    Mikael Nordfeldth <mmn@hethane.se>
 * @copyright 2014 Free Software Foundation http://fsf.org
 * @license   http://www.fsf.org/licensing/licenses/agpl-3.0.html GNU Affero General Public License version 3.0
 * @link      https://www.gnu.org/software/social/
 */

if (!defined('GNUSOCIAL')) { exit(1); }

/*
 * Dependencies:
 *  php5.5+
 *
 * Bugs:
 *  Mozilla Firefox doesn't support WebP. 
 *  Images generated seem to have a yellow/greenish tint. Dunno why.
 *
 * Comments:
 *  WebP thumbnails should probably be generated some other way. Like
 *  how VideoThumbnails are generated for example.
 */

class WebPThumbnailsPlugin extends Plugin
{
    public function initialize() {
        global $config;
        $config['attachments']['supported']['image/webp'] = 'webp';
    }

    public function onImageFileMetadata(ImageFile $imagefile)
    {
        if (!$this->detectWebP($imagefile->filepath)) {
            return true;
        }

        $image = imagecreatefromwebp($imagefile->filepath);
        $imagefile->width    = imagesx($image);
        $imagefile->height   = imagesy($image);
        $imagefile->type     = null;                // IMAGETYPE_WEBP is not yet defined
        $imagefile->mimetype = 'image/webp';        // though I think this is not official yet?
        $imagefile->alphatrans = true;

        imagedestroy($image);   // we could store it in memory, but I think it's better to clear

        return false;
    }

    protected function detectWebP($filepath)
    {
        if (!file_exists($filepath)) {
            throw new FileNotFoundException($filepath);
        }

        if (!$fh = fopen($filepath, 'rb')) {
            common_log(LOG_ERR, 'Could not open an existing file for reading: '.$filepath);
            return false;
        }

        // https://developers.google.com/speed/webp/docs/riff_container
        // First 4 bytes with ASCII characters 'R', 'I', 'F', 'F'
        if (!fread($fh, 4) === 'RIFF') {
            fclose($fh);
            return false;   // Not a RIFF file format
        }

        // Then the filesize in a 32bit unsigned int (from offset 8, so total size - 8 bytes)
        $filesize = filesize($filepath);
        $givensize = array_shift(unpack('V', fread($fh, 4)));
        if ($givensize !== $filesize-8) {
            fclose($fh);
            common_log(LOG_ERR, 'Supposed RIFF file type did not match expected filesize.');
            return false;   // If the filesize did not match what we expected.
        }

        // Then 4 bytes with ASCII characters 'W', 'E', 'B', 'P'
        if (!fread($fh, 4) === 'WEBP') {
            fclose($fh);
            common_debug('File was simply not a WebP image: '.$filepath);
            return false;
        }

        // Because all chunks are always even size, we can doublecheck that too.
        if ($givensize % 2 !== 0) {
            fclose($fh);
            common_log(LOG_ERR, 'Supposed WebP image file had uneven file size header.');
            return false;
        }

        // If we've gotten this far, it's a WebP image and we should be able to read it later..
        fclose($fh);
        return true;
    }

    public function onImageCreateFromPlugin(ImageFile $imagefile, &$image_src)
    {
        if ($imagefile->mimetype !== 'image/webp' || empty($imagefile->filepath)) {
            // We only open local files, so $imagefile->filepath must be set.
            return true;
        }

        $image_src = imagecreatefromwebp($imagefile->filepath);

        return false;
    }

    public function onImageSaveFromPlugin(ImageFile $imagefile, $image, $outpath)
    {
        if ($imagefile->mimetype !== 'image/webp') {
            // We only open local files, so $imagefile->filepath must be set.
            return true;
        }

        if (!imagewebp($image, $outpath)) {
            throw new ServerException('Could not save WebP image file to outpath!');
        }
        return false;
    }

    public function onStartShowAttachmentRepresentation(HTMLOutputter $out, File $attachment)
    {
        if (!$attachment->mimetype === 'image/webp') {
            return true;
        }

        $thumb = $attachment->getThumbnail(null, null, false, false);
        $out->element('img', array('class'=>'u-photo', 'src' => $thumb->getUrl(), 'alt' => ''));

        return false;
    }

    public function onPluginVersion(&$versions)
    {
        $versions[] = array('name' => 'WebP Thumbnails',
                            'version' => GNUSOCIAL_VERSION,
                            'author' => 'Mikael Nordfeldth',
                            'homepage' => 'https://www.gnu.org/software/social/',
                            'rawdescription' =>
                            // TRANS: Plugin description.
                            _m('Transitioning support for WebP thumbnails if you run PHP5.5+'));
        return true;
    }
}

